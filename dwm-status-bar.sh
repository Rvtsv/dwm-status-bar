#!/bin/sh
 	
#================================================#
# DWM Status Bar script.
# Creator: Rvtsv
# Date: 7/4/22
#================================================#

#Put false if you want no icons.
ICONSON="true"

#System Info
sysinfo(){
    local uname="$(uname -sr)"
    if [ "$ICONSON" = "false" ]; then
        echo " $uname "
    else
        echo " ⚙️ $uname "
    fi
}

#Current Date
date_(){
    local data="$(date '+%a %d-%b-%y')"
    if [ "$ICONSON" = "false" ]; then
        echo " $data "
    else
        echo " 🗓 $data "
    fi
}

#Current Time
time_(){
    local tempo="$(date '+%H:%M:%S')"
    if [ "$ICONSON" = "false" ]; then
        echo " $tempo "
    else
        echo " 🕓 $tempo "
    fi
}

#Battery Info
battery(){
    local id="BAT0"
    local cap="$(cat /sys/class/power_supply/$id/capacity)"
    local stat="$(cat /sys/class/power_spply/$id/status)"
    if [ "$ICONSON" = "false" ]; then
        echo " $stat ${cap}% "
    else
        if [ "$stat" = Charging ]; then
            echo " 🔌 ${cap}% "
        else
            echo " 🔋 ${cap}% "
        fi
    fi
}

#ALSA Volume Info
alsavol(){
    local vol="$(amixer sget Master | cut -d " " -f7 | tail -n1)"
    local stat="$(amixer sget Master | cut -d " " -f8 | tail -n1)"
    if [ "$ICONSON" = "false" ]; then
        if [ "$stat" = "[off]" ]; then
          echo " MUTE "
        else
          echo " VOL $vol "
        fi
    else
        if [ "$stat" = "[off]" ]; then
          echo " 🔇 "
        else  
          echo " 🔊 $vol "
        fi
    fi
 }

#Pulseaudio Volume Info
pulsevol(){
    local vol="$(pamixer --get-volume)"
    local mute="$(pamixer --get-mute)"
    if [ "$ICONSON" = "false" ]; then
        if [ "$mute" = "true" ]; then
          echo " MUTE "
        else
          echo " VOL [${vol}%] "
        fi
    else
        if [ "$mute" = "true" ]; then
          echo " 🔇 "
        else  
          echo " 🔊 [${vol}%] "
        fi
    fi
}

#Weather Info
#You can add your current location.
weather(){
    local location=""
    local wea="$(curl -s wttr.in/${location}?format=1 | cut -d " " -f4)"
    local symb="$(curl -s wttr.in/${location}?format=1 | cut -d " " -f1)"
    if [ "$ICONSON" = "false" ]; then
        echo " WEA $wea "
    else
        echo " $symb $wea "
    fi
}

#Resources Usage Info
#Put your storage(s) as your taste.
usage(){
    local disk="/dev/sdb2"
    local disk2="" 
    local ramu="$(free | awk '/Mem/ {printf "%dM/%dM\n", $3/1024, $2/1024}')"
    local cpuu="$(top -bn1 | awk '/Cpu/ {print $2}')"
    local dfu="$(df -h $disk | awk '{printf "%dG/%dG\n", $3, $2}' | tail -n1)"
    local dfu2="$(df -h $disk2 | awk '{printf "%dG/%dG\n", $3, $2}' | tail -n1)"
    if [ "$ICONSON" = "false" ]; then
        if [ -z "$disk2" ]; then
           echo " RAM $ramu CPU ${cpuu}% STO $dfu "
        else
           echo " RAM $ramu CPU ${cpuu}% STO $dfu STO2 $dfu2 "
        fi
    else
        if [ -z "$disk2" ]; then
           echo " 💻 RAM $ramu CPU ${cpuu}% STO $dfu "
        else
           echo " 💻 RAM $ramu CPU ${cpuu}% STO $dfu STO2 $dfu2 "
        fi
    fi
}

#Keyboard Lang
keyboard(){
    local kb="$(setxkbmap -query | awk '/layout/ {print $2}')"
    if [ "$ICONSON" = "false" ]; then
        echo " KB $kb "
    else
        echo " ⌨️ $kb "
    fi
}

#Edit to use it as your taste.
while true; do
    xsetroot -name "$(sysinfo)|$(usage)|$(pulsevol)|$(date_)|$(time_)" 
	sleep 1s
done
